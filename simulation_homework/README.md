# Human-robot interaction

1. Execute [MotomanVisualization.exe](./MotomanVisualization.exe)
2. Run the Simulink model.

When stopping the Simulink model, make sure the visualization is closed
beforehand or it will crash.